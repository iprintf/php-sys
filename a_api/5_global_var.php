<?PHP

// $GLOBALS['kyo'] = "hello world";

/*
 * foreach ($_SERVER as $key => $val) {
 *     echo $key, " = ", $val, '<br />', PHP_EOL;
 * }
 */

function dump($data, $title = "") {
    echo $title, "<br />", PHP_EOL;
    if (is_array($data)) {
        foreach ($data as $key => $val) {
            echo "&emsp;&emsp;  ", $key, " = ", $val, "<br />", PHP_EOL;
        }
    } else {
        echo $data, PHP_EOL;
    }
}

function main()
{
    echo "HTTP REQUEST: ",$_SERVER['REQUEST_METHOD'],"<br />", PHP_EOL;
    dump($_GET, "GET: ");
    dump($_POST, "POST: ");

    $files = in_array("file", array_keys($_FILES)) ? $_FILES['file'] : $_FILES;
    // $files = isset($_FILES['file']) ?? array();
    dump($files, "FILES: ");

    $put = file_get_contents("php://input");
    echo "put length: ", strlen($put), "<br />", PHP_EOL;
    echo $put, PHP_EOL;
    // file_put_contents("/tmp/up.png", $put);


// move_uploaded_file($_FILES['file']['tmp_name'], '/tmp/upload.file');
}

main();

