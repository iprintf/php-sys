#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $s = <<< EOF
    <div id="wrapper_wrapper">
        <div id="ftConw">
            <p id="lh">
                <a id="seth" onClick="h(this)" href="/">baidu.com home</a>
                <a id="setf" href="//www.baidu.com/cache/sethelp/index.html" target="_blank">baidu.com home</a>
                <a onmousedown="return ns_c({'fm':'behs','tab':'tj_about'})" href="http://home.baidu.com">about baidu.com</a>
                <a onmousedown="return ns_c({'fm':'behs','tab':'tj_about_en'})" href="http://ir.baidu.com">About Baidu</a>
            </p>
            <p id="cp">&copy;2014&nbsp;Baidu&nbsp;<a href="/duty/" name="tj_duty">readme</a>&nbsp;&nbsp;<img src="http://s1.bdstatic.com/r/www/cache/static/global/img/gs_237f015b.gif"></p>
        </div>
    </div>
EOF;

    echo $s, PHP_EOL;

    $dom = new DOMDocument("1.0", "UTF-8");
    $dom->loadHTML($s);
    $node = $dom->getElementById('ftConw');

    echo 'nodeName: ', $node->nodeName, PHP_EOL;
    // echo 'nodeValue: ', $node->nodeValue, PHP_EOL;
    echo 'nodeType: ', $node->nodeType, PHP_EOL;
    echo 'parent nodeName: ', $node->parentNode->nodeName, PHP_EOL;
    echo 'parent id: ', $node->parentNode->getAttribute('id'), PHP_EOL;
    echo 'parent child 1 id: ', $node->parentNode->childNodes->item(1)->getAttribute('id'), PHP_EOL;
    echo $node->childNodes->length, PHP_EOL;

    for ($i = 0; $i <= $node->childNodes->length; ++$i) {
        $n = $node->childNodes->item($i);
        if ($n instanceof DOMElement && $n->nodeType === 1)
            echo $n->getAttribute('id'), PHP_EOL;
    }

    return 0;
}

exit(main($argc, $argv));

