<?PHP

function add($n1, $n2)
{
    return $n1 + $n2;
}

function getString()
{
    return "kyo soap string!";
}


$srv = new SoapServer(null, array(
        // 'location' => 'http://3.3.3.9:81/soapsrv.php',
        'location' => 'http://3.3.3.9/soapsrv.php',
        'uri'  => 'soapsrv.php'
    ));

$srv->addFunction('add');
$srv->addFunction('getString');

$srv->handle();

