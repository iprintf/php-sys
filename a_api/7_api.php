<?PHP

function baidu_api($url) {
    $ch = curl_init();

    $header = array(
                'apikey:37a3919aad4704950d45602281beae11',
                'Accept-Charset:UTF-8'
            );

    curl_setopt($ch, CURLOPT_HTTPHEADER  , $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // 执行HTTP请求
    curl_setopt($ch , CURLOPT_URL , $url);
    $res = curl_exec($ch);

    $info = curl_getinfo($ch);

    curl_close($ch);

    if ($info['http_code'] >= 200 && $info['http_code'] <= 299)
        return json_decode($res);

    return FALSE;
}

//IP查询
// $url = 'http://apis.baidu.com/apistore/iplookupservice/iplookup?ip=223.152.25.66';
//天气查询
$url = 'http://apis.baidu.com/apistore/weatherservice/';
$city = urlencode("深圳");
$cityid = '101280601';
$name_en = 'shenzhen';
//查询可用城市信息
// $url .= 'citylist?cityname='.$city;
//查询历史7天和未来4天和当天的天气情况
$url .= 'recentweathers?cityname='.$city.'&cityid='.$cityid;

$s = file_get_contents('./base64.file');

echo file_put_contents('/tmp/baidu.jpg', base64_decode($s)), PHP_EOL;
// $file = file_get_contents("/www/upload.png");
// echo base64_encode($file);

// print_r(baidu_api($url));

