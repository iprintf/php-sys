<?php

function main()
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "http://php.com/5_global_var.php?name=kyo&age=23");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    $data = array(
        'pname' => 'mary',
        'page' => 67
    );
    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
    //
    //POST DELETE PUT POST 请求正文的数据
    // curl_setopt($ch, CURLOPT_POSTFIELDS, "postname=tom&postage=38");
    // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

    //关闭安全上传, 不允许使用@路径方式上传文件
    //5.6默认为开启动, PHP7直接强制关闭, 无法开启
    // curl_setopt($ch, CURLOPT_SAFE_UPLOAD, FALSE);
    //调用使用，将HTTP头信息输出
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, array('file' => '@/web/bin/lamp'));

    // PHP7安全上传方式
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('file' => curl_file_create('/www/upload.png', 'images/png', "abc.png")));

    //PUT 上传文件方式
    // $path = "/www/upload.png";
    // $fp = fopen($path, "r");
    // curl_setopt($ch, CURLOPT_PUT, true);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:images/png'));
    // curl_setopt($ch, CURLOPT_INFILESIZE, filesize($path));
    // curl_setopt($ch, CURLOPT_INFILE, $fp);
    // curl_setopt($ch, CURLOPT_UPLOAD, true);

    $s = curl_exec($ch);

    // fclose($fp);

    echo $s, PHP_EOL;

    curl_close($ch);

    return 0;
}

main();

