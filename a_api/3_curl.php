<?php

//curl_init         初始化curl会话
//curl_setopt       设置会话选项
//curl_exec         执行curl会话
//curl_close        关闭会话

$a = "hello";

function main($argc, & $argv)
{
    echo $GLOBALS['a'];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "http://www.baidu.com");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $s = curl_exec($ch);

    curl_close($ch);

    return 0;
}

exit(main($argc, $argv));

