#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    echo time(), PHP_EOL;
    echo mktime(0, 0, 0, 10, 12, 2015), PHP_EOL;

    echo microtime(), PHP_EOL;
    echo microtime(true), PHP_EOL;

    echo strtotime("+1 days"), PHP_EOL;

    echo date("Y-m-d", strtotime("+1 month")), PHP_EOL;


    $passwd = '$6$Y1ozRdGr$rMH8uksKugXq2vGF9LliOna0oP.odEhiFa3caEuXVYF5GleeTjQ1giaAgebGLMxuikpuQpBCI8S.APzI4nP9Y.';
    $str = crypt("1234", $passwd);

    if ($passwd === $str)
        echo "登录成功!\n";

    return 0;
}

exit(main($argc, $argv));

