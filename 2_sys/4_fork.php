#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    echo "parent: ", posix_getpid(), PHP_EOL;

    $pid = pcntl_fork();
    if ($pid == 0) {
        putenv("KYO=hello");
        printf("create child pid = %d\n", posix_getpid());
        pcntl_exec("./3_env.php", array(11, 22, 33, 44));
        // exit(99);
    }
    echo "wait.....!\n";
    printf("wait pid = %d\n", pcntl_wait($status));
    echo "exit return : ", pcntl_wexitstatus($status), PHP_EOL;
    echo "isexit : ", pcntl_wifexited($status), PHP_EOL;
    echo "iskill : ", pcntl_wifsignaled($status), PHP_EOL;
    // echo ($status >> 8), PHP_EOL;

    echo "parent: child pid = ", $pid, PHP_EOL;

    fgets(STDIN);


    return 0;
}

exit(main($argc, $argv));

