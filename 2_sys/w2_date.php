#!/usr/bin/php
<?php

function isYear($year)
{
    if ($year % 400 == 0
        || $year % 4 == 0 && $year % 100 != 0)
        return true;
    return false;
}

const MONTH = [
    [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
];

function getWorkDay($holiday, $overtime, $month = 0)
{
    if ($month < 1 || $month > 12) {
        $startTime = strtotime($holiday[0]);
        $endTime = strtotime(date('Y1231', $startTime));
    } else {
        $year = substr($holiday[0], 0, 4);
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);
        $endDay = MONTH[isYear($year)][$month - 1];
        $startTime = strtotime($year.$month.'01');
        $endTime = strtotime($year.$month.$endDay);
    }

    for ($workDay = array(); $startTime <= $endTime; $startTime += 86400) {
        $week = date("w", $startTime);
        $date = date("Ymd", $startTime);
        if ((($week == 0 || $week == 6) || in_array($date, $holiday))
                && !in_array($date, $overtime))
            continue;

        $workDay[] = $startTime;
    }

    // echo date("Y-m-d", $startTime), PHP_EOL;
    // echo date("Y-m-d", $endTime), PHP_EOL;

    return $workDay;
}

function main($argc, & $argv)
{
    $holiday = array(
        '20160101',
        '20160207',
        '20160208',
        '20160209',
        '20160210',
        '20160211',
        '20160212',
        '20160213',
        '20160404',
        '20160502',
        '20160609',
        '20160610',
        '20160915',
        '20160916',
        '20161001',
        '20161002',
        '20161003',
        '20161004',
        '20161005',
        '20161006',
        '20161007'
    );

    $overtime = array(
        '20160206',
        '20160214',
        '20160612',
        '20160918',
        '20161008',
        '20161009'
    );

    foreach (getWorkDay($holiday, $overtime, 2) as $time) {
        echo date('Y-m-d w', $time), PHP_EOL;
    }

    return 0;
}

exit(main($argc, $argv));

