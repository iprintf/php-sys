#!/usr/bin/php
<?php

function isPalindromic(int $num) : bool
{
    if ($num < 10)
        return FALSE;

    for ($n = $num, $sum = 0; $n > 0; $n = (int)($n / 10))
    {
        $sum = $sum * 10 + $n % 10;
    }

    if ($num == $sum)
        return TRUE;

    return FALSE;
}

function getPalindromicNum(int $start, int $end): int
{
    $count = 0;

    while ($start < $end) {
        if (isPalindromic($start)) {
            printf("child pid = %d, %d\n", posix_getpid(), $start);
            $count++;
        }
        $start++;
    }

    return $count;
}

function main($argc, & $argv)
{
    $sep = (5000 - 1000) / 10;

    for ($i = 0; $i < 10; ++$i) {
        if (pcntl_fork() === 0) {
            exit(getPalindromicNum(
                    1000 + $i * $sep,
                    1000 + ($i + 1) * $sep));
        }
    }

    for ($i = 0, $sum = 0; $i < 10; ++$i) {
        pcntl_wait($status);
        $sum += pcntl_wexitstatus($status);
    }

    printf("sum = %d\n", $sum);

    return 0;
}

exit(main($argc, $argv));

