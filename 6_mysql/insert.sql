--  create database if not exists company;

--  use company;

drop table if exists emp;
create table emp (
    empno integer primary key,
    ename varchar(20),
    sex char(1),
    birthday date,
    hiredate date,
    sal decimal(10,2),
    deptno tinyint(1),
    managerno int);

insert into emp values
(1,'boss','m','1964-08-08','1995-01-01','20000','1','1'),
(2,'zhangsan','m','1967-04-05','1995-04-11','15000','2','1'),
(3,'lisi','f','1973-01-28','1998-11-21','13000','3','1'),
(4,'wangwu','f','1975-06-03','1999-12-12','12000','4','1'),
(5,'maliu','m','1982-08-18','2001-07-03','8000','2','2'),
(6,'tianqi','f','1983-02-15','2002-11-01','7000','2','2'),
(7,'mark','m','1984-08-12','2003-10-02','6500','3','3'),
(8,'john','m','1985-09-14','2005-04-03','6000','3','3'),
(9,'mm','f','1990-06-08','2008-09-13','4000','4','4');

drop table if exists dept;
create table dept (deptno tinyint(1),
                   deptname varchar(30),
                location varchar(50));

insert into dept values (1,'manager','beijing'),
                        (2,'it','shenzhen'),
                        (3,'sale','shanghai'),
                        (4,'services','guangzhou');
