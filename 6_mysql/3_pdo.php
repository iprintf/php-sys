#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    // $db = new PDO('mysql:host=3.3.3.9;port=3306;dbname=company', 'company', 'abc');
    $db = new PDO('sqlite:company.db');

    if (!$db) {
        fprintf(STDERR, "pdo connect failed!\n");
        return 1;
    }

    $result = $db->query("select * from emp");

    $data = $result->fetchAll(PDO::FETCH_ASSOC);

    foreach ($data as $row) {
        foreach ($row as $col) {
            echo $col, " ";
        }
        echo PHP_EOL;
    }

    return 0;
}

exit(main($argc, $argv));

