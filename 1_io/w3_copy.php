#!/usr/bin/php
<?php

/*
 * 1. 源路径有问题
 * 2. 源是文件
 *     目标路径有问题，提示错误
 *     目标是不存在的文件, 创建文件
 *     目标是存在的文件, 提示是否覆盖
 *     目标是目录, 复制目录里
 * 3. 源是目录
 *     目标路径有问题，提示错误
 *     目标是不存在的目录， 创建目录
 *     目标是已存在的目录, 将源复制到此目录里
 *     目标是文件， 提示错误
 */

function _copy_dir($src, $dst)
{
    if (!mkdir($dst)) {
        fprintf(STDERR, "创建 %s 失败!\n", $dst);
        return false;
    }

    $dp = opendir($src);
    if (!$dp) {
        fprintf(STDERR, "%s 打开失败!\n", $src);
        return false;
    }

    while (($file = readdir($dp)) != FALSE) {
        if ($file === "." || $file === "..")
            continue;
        $srcpath = $src.DIRECTORY_SEPARATOR.$file;
        $dstpath = $dst.DIRECTORY_SEPARATOR.$file;
        if (is_dir($srcpath))
            return _copy_dir($srcpath, $dstpath);
        else
            copy($srcpath, $dstpath);
    }

    closedir($dp);

    return true;
}

function copy_dir($src, $dst)
{
    if (is_file($dst)) {
        fprintf(STDERR, "目标不是已存在的文件!\n");
        return false;
    }
    if (file_exists(dirname($dst)) === FALSE) {
        fprintf(STDERR, "目标路径有问题!\n");
        return false;
    }
    $filename = basename($src);
    //组合目标路径，不存在的目录路径
    while (is_dir($dst)) {
        $dst .= DIRECTORY_SEPARATOR.$filename;
        if (is_file($dst)) {
            fprintf(STDERR, "目标不是已存在的文件!\n");
            return false;
        }
    }

   return  _copy_dir($src, $dst);
}

function copy_file($src, $dst)
{
    if (is_dir($dst)) {
        $dst .= DIRECTORY_SEPARATOR.basename($src);
    } else if (file_exists(dirname($dst)) === FALSE) {
            fprintf(STDERR, "目标路径有问题!\n");
            return false;
    }

    if (file_exists($dst)) {
            printf("是否覆盖(Y/N): ");
            $ch = fgetc(STDIN);
            if (!($ch === 'Y' || $ch === 'y'))
                return true;
    }

    return copy($src, $dst);
}

function main($argc, & $argv)
{
    if ($argc < 3) {
        fprintf(STDERR, "参数个数不足!\n");
        return 1;
    }
    $stype = filetype($argv[1]);

    if ($stype === FALSE) {
        fprintf(STDERR, "源路径有问题!\n");
        return 2;
    } else if ($stype == 'dir') {
        return (int)copy_dir($argv[1], $argv[2]);
    }

    return (int)copy_file($argv[1], $argv[2]);
}

exit(main($argc, $argv));

