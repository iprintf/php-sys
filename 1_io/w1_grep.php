#!/usr/bin/php
<?php

echo sprintf("hello %d world %f kyo!", 11, 34.56), PHP_EOL;


set_exception_handler(function(Exception $e){
    echo $e->getMessage(), PHP_EOL;
    exit($e->getCode());
});

function main($argc, & $argv)
{
    if ($argc < 3)
        throw new Exception("./grep key file!", 12);

    $fp = @fopen($argv[2], "r");
    if ($fp === FALSE)
        throw new Exception($argv[2]." 打开失败!", 13);

    $l = 0;
    while (($s = fgets($fp)) != FALSE) {
        ++$l;
        // echo strpos($s, $argv[1]), PHP_EOL;
        if (strpos($s, $argv[1]) !== FALSE) {
            printf("\033[32m%d\033[34m:\033[0m", $l);
            echo str_replace($argv[1], "\033[1;31m".$argv[1]."\033[0m", $s);
            // printf("%d:%s", $l, $s);
        }
    }

    fclose($fp);

    return 0;
}

exit(main($argc, $argv));

