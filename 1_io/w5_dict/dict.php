#!/usr/bin/php
<?php

require_once __DIR__.DIRECTORY_SEPARATOR."vt.php";
require_once __DIR__.DIRECTORY_SEPARATOR."Input.php";

class dict
{
    private $input = null;
    private $data = null;
    private $s = "";
    private $len = 0;
    private $like_max = 10;

    public function __construct($path = "./ciku.dict")
    {
        $this->input = new Input();

        $this->data = static::loadDict($path);
        if ($this->data === null) {
            fprintf(STDERR, "加载词库失败!\n");
            return null;
        }

        vt::init();
        vt::output("请输入查询单词: ");
        vt::cursor(vt::CUR_SAVE);
    }

    public function __destruct()
    {
        unset($this->input);
        vt::init();
    }

    static public function loadDict($path = "./ciku.dict")
    {
        $fp = fopen($path, "r");
        if (!$fp)
            return null;

        $dict = array();

        $en = null;

        while (($line = fgets($fp)) != FALSE) {
            if ($en == null) {
                $en = trim($line);
            } else {
                $dict[$en] = trim($line);
                $en = null;
            }
        }

        fclose($fp);

        return $dict;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function bs()
    {

        if ($this->len < 1)
            return false;


        vt::bs();
        $this->s = substr($this->s, 0, --$this->len);

        // vt::debug(sprintf("bs len = %d\n", $this->len), 10);

        return true;
    }

    public function save($key)
    {
        if ($this->len > 20)
            return false;

        $this->s .= chr($key);
        $this->len++;


        return true;
    }

    public function find()
    {
        vt::gotoxy(2, 1);
        vt::clean(vt::CLEAN_LINEALL);
        vt::move();
        vt::clean(vt::CLEAN_LINEALL);
        vt::move(1, vt::DIR_UP);

        if (!array_key_exists($this->s, $this->data)) {
            printf("\t %s 找不到!\n", $this->s);
        } else {
            printf("\ten: %s\n", $this->s);
            printf("\tcn: %s\n", $this->data[$this->s]);
        }
        vt::cursor(vt::CUR_LOAD);
        vt::clean(vt::CLEAN_LINE);
        $this->s = "";
        $this->len = 0;
        fflush(STDOUT);

        return false;
    }


    public function keyhandle($key, & $obj)
    {
        if ($key == Input::TAB
                || $key == Input::UP
                || $key == Input::DOWN
                || $key == Input::LEFT
                || $key == Input::RIGHT)
            return;

        if (($key == Input::BS && !$obj->bs())
                || ($key == Input::ENTER && !$obj->find())
                || $key != Input::BS && !$obj->save($key))
            return;

        $obj->show();
    }

    public function like()
    {
        $count = 0;
        foreach ($this->data as $key => $val) {
            if (strncmp($key, $this->s, $this->len) === 0) {
                echo $key, PHP_EOL;
                if (++$count == $this->like_max)
                    break;
            }
        }
    }

    public function show()
    {
        vt::cl($this->like_max, 5);
        $this->like();
        printf("%s%s", vt::cursor(vt::CUR_LOAD, true), $this->s);
    }

    public function run()
    {
        $this->input->run(array("dict", "keyhandle"), $this);
    }
}


function main($argc, & $argv)
{
    $dict = new dict();
    if ($dict)
        $dict->run();

    return 0;
}

exit(main($argc, $argv));

