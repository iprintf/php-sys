#!/usr/bin/php
<?PHP

set_exception_handler(function(Exception $e) {
    echo $e->getMessage(), PHP_EOL;
    exit($e->getCode());
});

// error_reporting(0);


echo getcwd(), PHP_EOL;
// chdir("/Videos");
echo getcwd(), PHP_EOL;

$dp = opendir(".");
if ($dp === FALSE)
    throw new Exception("文件打开失败!", 1);

while (($file = readdir($dp)) != FALSE) {
    // if ($file == "." || $file == "..")
    // if (fnmatch(".*", $file))
    if ($file[0] == '.')
        continue;
    echo $file, PHP_EOL;
}
echo "======== rewinddir =================", PHP_EOL;
rewinddir($dp);
while (($file = readdir($dp)) != FALSE) {
    // if ($file == "." || $file == "..")
    // if (fnmatch(".*", $file))
    if ($file[0] == '.')
        continue;
    echo $file, PHP_EOL;
}

closedir($dp);

echo "======== DirectoryIterator ==========", PHP_EOL;

foreach (new DirectoryIterator(".") as $v) {
    if (!$v->isDot())
        echo $v->getFileName(), PHP_EOL;
}

echo "======== glob ==========", PHP_EOL;
print_r(glob("./*.php"));

echo "======== stat ===========", PHP_EOL;

/*
 * 1_file.php   33277
 *     -       0100000
 *     rwx     0700
 *     rwx     070
 *     r-x     04 | 01
 *     0100000 | 0700 | 070 | 04 | 01
 *     0100775
 *     41471
 *          0120000 | 0700 | 070 | 07
 *          0120777
 *
 */

if ((41471 & 0170000) == 0120000)
    echo "hello\n";

// print_r(stat("1_file.php"));
// print_r(lstat("link"));

