#!/usr/bin/php
<?php


function main($argc, & $argv)
{
    $fp = fopen("./A", "r");
    fscanf($fp, "%d", $num);
    fclose($fp);
    /*
     * echo $num, PHP_EOL;
     * $num = (int)file_get_contents("./A");
     * echo $num, PHP_EOL;
     */
    $fp = fopen("./B", "r");

    $s = ord('a');
    $e = $s + 26;
    $S = ord('A');
    $E = $S + 26;
    $w = '';
    while (($ch = fgetc($fp)) !== FALSE) {
        $n = ord($ch);
        if ($n >= $s && $n <= $e
                || $n >= $S && $n <= $E) {
            $w .= $ch;
        } else if ($w !== '') {
            if (--$num === 0)
                break;
            $w = '';
        }
    }
    echo $w, PHP_EOL;

    fclose($fp);

    return 0;
}

exit(main($argc, $argv));

