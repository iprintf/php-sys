#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $sd = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if (!$sd) {
        echo socket_strerror(socket_last_error($sd)), PHP_EOL;
        return 0;
    }

    socket_connect($sd, "3.3.3.9", 9999);

    $str = socket_write($sd, $argv[1], strlen($argv[1]));

    socket_close($sd);

    return 0;
}

exit(main($argc, $argv));

