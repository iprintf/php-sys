#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $sd = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    if (!$sd) {
        echo socket_strerror(socket_last_error($sd)), PHP_EOL;
        return 0;
    }

    if (!socket_bind($sd, "3.3.3.9", 9999)) {
        echo socket_strerror(socket_last_error($sd)), PHP_EOL;
        return 0;
    }

    /*
     * echo "select start", PHP_EOL;
     * $ret = socket_select($r, $w, $e, 10);
     * echo "select end: ", $ret, PHP_EOL;
     */

    while (1)
    {
        $r = array($sd);
        $ret = socket_select($r, $w, $e, 3);
        if ($ret > 0) {
            socket_recvfrom($sd, $buf, 1024, 0, $ip, $port);
            echo $ip, "[", $port, "]: ", $buf, PHP_EOL;
        }
        else if ($ret == 0)
            break;
    }

    socket_close($sd);

    return 0;
}

exit(main($argc, $argv));

