#!/usr/bin/php
<?php

const MAX = 1024;

function main($argc, & $argv)
{
    $sd = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    if (!$sd) {
        echo socket_strerror(socket_last_error($sd)), PHP_EOL;
        return 0;
    }

    $file = file_get_contents($argv[1]);
    $total = strlen($file);

    $start = 0;
    $count = 0;
    while ($start < $total) {
        $s = substr($file, $start, MAX);
        $len = socket_sendto($sd, $s, strlen($s), 0, "3.3.3.9", 9999);
        printf("send[%d]: %d\n", ++$count, $len);
        $buf = socket_read($sd, MAX);
        if ($buf != $len)
            break;
        $start += $len;
    }

    socket_close($sd);

    return 0;
}

exit(main($argc, $argv));

