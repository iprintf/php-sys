#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $sd = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    if (!$sd) {
        echo socket_strerror(socket_last_error($sd)), PHP_EOL;
        return 0;
    }

    if (!socket_bind($sd, "3.3.3.9", 9999)) {
        echo socket_strerror(socket_last_error($sd)), PHP_EOL;
        return 0;
    }

    while (1)
    {
        socket_recvfrom($sd, $buf, 1024, 0, $ip, $port);
        echo $ip, "[", $port, "]: ", $buf, PHP_EOL;
    }

    socket_close($sd);

    return 0;
}

exit(main($argc, $argv));

