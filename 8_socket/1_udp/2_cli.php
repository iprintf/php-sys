#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $sd = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    if (!$sd) {
        echo socket_strerror(socket_last_error($sd)), PHP_EOL;
        return 0;
    }

    $str = socket_sendto($sd, $argv[1], strlen($argv[1]), 0, "3.3.3.9", 9999);

    socket_close($sd);

    return 0;
}

exit(main($argc, $argv));

