#!/usr/bin/php
<?php

require_once __DIR__.DIRECTORY_SEPARATOR.'scp.php';

function main($argc, & $argv)
{
    $port = 9999;
    $share = null;

    if ($argc > 1)
        $port = $argv[1];

    if ($argc > 2)
        $share = $argv[2];

    $scp = new scp('0.0.0.0', $port);
    $scp->server($share);

    fgets(STDIN);

    $scp->stop_srv();

    return 0;
}

exit(main($argc, $argv));

