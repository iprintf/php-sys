#!/usr/bin/php
<?php

require_once __DIR__.DIRECTORY_SEPARATOR.'scp.php';

function getpass($prompt = "Password: ")
{
    echo $prompt;
    system('stty -echo');
    $s = rtrim(fgets(STDIN));
    system('stty echo');
    echo PHP_EOL;
    return $s;
}

function main($argc, & $argv)
{
    // ./scp root@3.3.3.9:9999:/file.txt file.txt
    if ($argc <= 2) {
        fprintf(STDERR, "./scp user@ip:port:srcpath dstpath!\n");
        return 1;
    }
    if (file_exists($argv[2])) {
        printf("是否覆盖(Y/N): ");
        $ch = trim(fgets(STDIN));
        if (!($ch == 'Y' || $ch == 'y'))
            return 0;
    }

    $passwd = getpass();

    $user = explode('@', $argv[1]);
    $m = explode(':', $user[1]);
    $user = $user[0];

    $scp = new scp($m[0], $m[1]);
    $connect = $user."\n".$passwd."\n".$m[2];
    $packet = $scp->client($connect, $argv[2]);

    return 0;
}

exit(main($argc, $argv));

