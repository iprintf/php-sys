#!/usr/bin/php
<?php

require_once __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'udp.php';

function main($argc, & $argv)
{
    $udp = new udp('3.3.3.9', 9999);
    $packet = $udp->cli($argv[1]);
    echo 'cli connect ack data: ', $packet, PHP_EOL;

    $ret = $udp->send(null, udp::PACKET_ALL, "filename data send!");
    echo 'cli send data: ', $ret, PHP_EOL;
    $packet = $udp->recv(null, udp::PACKET_DISCONNECT);
    echo 'cli dis connect: ', $packet, PHP_EOL;

    return 0;
}

exit(main($argc, $argv));

