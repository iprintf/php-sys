#!/usr/bin/php
<?php

require_once __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'udp.php';

function cli_handle($cli, $obj, $data)
{
    printf("recv[%s:%d] conn: %s\n", $cli['addr'],
             $cli['port'], $cli['conn']);
    $ret = $obj->send($cli['sd'], udp::PACKET_CONN_ACK, "OK");
    echo 'srv connect ack write: ', $ret, PHP_EOL;
    $packet = $obj->recv($cli['sd']);
    // var_dump($packet);
    echo 'cli data packet: ', $packet['data'], PHP_EOL;
    $ret = $obj->send($cli['sd'], udp::PACKET_DISCONNECT, "end");
    echo 'srv dis connect write: ', $ret, PHP_EOL;
}

function main($argc, & $argv)
{
    $udp = new udp('3.3.3.9', 9999);
    $udp->srv("cli_handle");

    fgets(STDIN);

    $udp->stop_srv();

    return 0;
}

exit(main($argc, $argv));

