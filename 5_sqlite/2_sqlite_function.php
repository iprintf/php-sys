#!/usr/bin/php
<?php

CONST SEX = array('女', '男');

function main($argc, & $argv)
{
    $db = new SQLite3('kyo.db');

    $db->createFunction('kyo', function($val){
        return  date("Y-m-d", $val);
    });

    $db->createFunction('sex', function($val){
        return SEX[$val];
    });

    $db->createAggregate('hello', function(& $context, $row, $str){
        $context += $str;
        return $context;
    }, function($context){
        return $context;
    });

    echo $db->querySingle("select sex(sex) from student limit 1"), PHP_EOL;

    echo $db->querySingle("select kyo(time) from grade limit 1"), PHP_EOL;
    echo $db->querySingle("select hello(id) from grade"), PHP_EOL;

    $db->close();

    return 0;
}

exit(main($argc, $argv));

