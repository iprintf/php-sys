#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $db = new SQLite3('kyo.db');

    // $stm = $db->prepare('INSERT INTO class (name, num, sid) VALUES (?, ?, ?)');
    // $stm->bindParam('1', $name);
    // $stm->bindParam('2', $num);
    // $stm->bindParam('3', $sid);
    $stm = $db->prepare('INSERT INTO class (name, num, sid) VALUES (:name, :num, :sid)');
    $stm->bindParam(':name', $name);
    $stm->bindParam(':num', $num);
    $sid = 1;
    $stm->bindValue(':sid', $sid);

    $name = 'PHP';
    $num = 5;
    $stm->execute();

    $name = 'JAVA';
    $num = 10;
    $stm->execute();

    $name = 'WEB';
    $num = 15;
    $stm->execute();

    $stm->close();

    $db->close();

    return 0;
}

exit(main($argc, $argv));

