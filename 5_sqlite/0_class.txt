<?php

sqlite数据库

sqlite概述 {
    设计目标：简单 灵活 紧凑 快速 整体上的易用
    sqlite是开源的嵌入式关系型数据库(RDBMS) 没有独立运行的进程
    基本支持ANSI SQL92标准子集(DDL DML DQL DCL 事务、视图、子查询等)
    支持RDBMS特性: 触发器、索引、自动增加字段 限定子句

    特性
        零配置 移植性 开源(自由授权)

        不需要网络配置和管理(DBA)
        不担心防火墙和地址解析
        不用浪费时间管理复杂的授权和权限
        SQLite数据库只是普通磁盘文件, 采用跨平台兼容二进制
        非常简单的API(使用C实现)
        不需要外部服务器，占用空间小 执行效率高
        非常适应中小型项目

        SQlite的局限性: 并发和网络环境

        SQLite未能实现的特性:
            完整的触发器支持
            完整的修改表结构支持
            右外接连和全外连接查询的支持
            可更新视图
            窗口功能
            授权和撤销

    体系结构
        接口        由C语言编写的API
        编译器
            词法分析器
            语法分析器
            代码生成器

        虚拟机
            SQLite核心
            执行编译后的操作码(引擎)， 字节码上工作， 独立CPU架构和操作

        后端
            B-tree              树型结构, 树的叶子叫页面
            页缓冲              帮助B-tree管理页面, 负责传输
            操作系统接口        封装OS文件API

        工具
            工具模块包含公共服务(字符串比较 Unicode转换等功能)
}

SQLite的使用 {
    官网: www.sqlite.org

    sqlite组成部分:
        sqlite命令行程序(CLP)
            不依赖sqlite共享库，通过命令行操作数据库的工具
        sqlite共享库(.so|.dll)
        sqlite analyzer分析工具  性能优化工具
        TCL扩展                 TCL脚本语言扩展

    sqlite源码包:
        zip格式         windows使用
        tar格式         linux、UNIX使用(包括autoconf工具)

    sqlite源码编译：
        如果想在sqlite3交互模式使用vi操作方法
            sudo apt install libreadline-dev

        设置readline交互模式使用vir操作方式
            vim ~/.inputrc
                set editing-mode vi
                set keymap vi

        ./configure --enable-readline --disable-editline
        make clean
        make -j4
        sudo make install
        测试： 可以运行sqlite3命令代码安装成功

    或者下载已编译好的sqlite3工具
        unzip sqlite-tools-linux-x86-3150000.zip
        sudo cp sqlite-tools-linux-x86-3150000/sqlite3 /usr/local/bin/
        运行sqlite3命令即可

        如果是64位系统要确保安装32兼容库才能运行
            apt install libc:i386

    sqlite3命令行工具的使用 {
        格式：sqlite3 [选项] 数据库文件路径 [SQL语句]
        运行会进入命令行交互模式(sqlite shell)
        输入以.开头的字符串会当作sqlite3工具的命令
        没有以.开头的字符串会当作SQL语句来解析, 每个SQL语句必须以分号结尾

        sqlite3工具的命令
            .help               列出所有命令的帮助
            .exit               退出交互模式
            .quit(.q)           退出交互模式
            .log                是否开启日志(文件路径/stderr/stdout/off)
            .databases          列出附加数据库信息
            .tables             列出当前数据库的所有表
            .indices            列出所有索引或某个表的索引
            .schema             显示DDL语句(sqlite_master表)
            .promat             设置sqlite3命令提示符
            .colseparator       设置sqlite3的列分隔符
            .rowseparator       设置sqlite3的行分隔符
            .show               显示当前sqlite各种配置
            .echo   on/off      是否开启命令回显
            .headers on/off     是否开启列名显示
            .mode               设置输出模式
                csv     逗号分隔值
                column  左对齐的列
                html    html表格代码
                insert  显示insert SQL语句
                line    一行一个数据
                list    以.colseparator指定分隔符分隔值(默认)
                tabs    由tabs分隔值
                tcl     TCL列表元素
            .width              设置column模式每列宽度
                .width 2 3 5
            .nullvalue          设置null值输出
            .output             设置输出到哪
                .output stdout  设置输出到标准输出
                .output file    设置输出到file文件当中
            .print              输出字符串
            .backup             备份数据库，后面备份路径
            .dump               导出数据(sql语句)
                .output dump.sql
                .dump
                不用交互导出数据
                    sqlite3 数据库文件 '.dump' > dump.sql
            .read               导入.dump导出数据
                sqlite3 test.db < dump.sql
                或
                sqlite3 -init dump.sql test.db .exit
            .stats  on/off      是否开启统计信息
            .timer  on/off      是否开启cpu定时器测量
            .bail   on/off      发生错误是否马上停止
    }
}

SQL语言 {
    语法规则 {
        SQL: 结构化查询语句
            DQL(data query language)
                数据查询语言    select
            DDL(data define language)
                数据定义语言    create drop alter truncate rename
            DML(data manipulate language)
                数据操作语言    insert update delete
            DCL(data control language) (sqlite不支持)
                数据控制语言    grant revoke

        sqlite不是严格区别大小写, 某些子句是大小写敏感(glob子句)
        每条SQL语句都要以分号结尾

        常量
            字符串常量
                SQL标准为单引号引起来的内容
                常量值大小写敏感
            数字常量
                整数、十进制数和科学记数法

            二进制常量
                x'十六进制'  前面x前缀大小写无所谓

        注释
            单行注释    --      可以加任意位置
            多行注释    /**/
    }

    sqlite数据类型 {
        sqlite支持原始数据类型有5个，称为存储类
            NULL        值只有null, 占位使用
                null不是0,也不是空字符串
                select typeof(null);
            INTEGER     值为一个带符号的整型
                根据值的大小存储空间为1 2 3 4 6和8个字节
                值没有引号引起来并且不带小数和指数就会指派为INTEGER
                select typeof(12354);
            REAL        值为一个浮点数 存储空间为8个字节
                值没有引号引起来并且带小数和指数就会指派为REAL
                select typeof(12354.354);
            TEXT        值为文本字符串(UTF-8)
                只要单引号引起来的数据，会被指派为TEXT
                select typeof('12354.354');
            BLOB        值为二进制数据,完全根据输入存储
                值X'十六进制数'/x'十六进制数', 会被指派BLOB
                select typeof(X'ABCD');

        sqlite是弱类型, 根据值推断存储类型
            NULL < INTEGER < REAL < TEXT < BLOB

        类型的亲和性(Type Affinity)
            sqlite给字段声明类型，该字段实际仅仅具有了该类型的亲和性
            sqlite提供类型的亲和性
                TEXT    该列使用NULL TEXT和BLOB存储类
                    VARYING, TEXT, CLOB, VARCHAR(255),
                    CHARACTER(20), CHARACTER(255),
                    NVARCHAR(100), NATIVE

                NUMERIC 该列可能使用所有存储类
                    NUMERIC, DECIMAL(10, 5), BOOLEAN,
                    DATE, DATETIME

                INTEGER 与NUMERIC相同, 表达式带有异常
                    INT, NTEGER, INTYINT, SMALLINT,
                    MEDIUMINT, BIGINT,
                    UNSIGNED BIG INT, INT2, INT8

                REAL    与NUMERIC相同，强制把整型转成浮点形式
                    REAL、DOUBLE、FLOAT

                NONE    不会强制转化存储类
                    BLOB

            例: sqlite没有提供boolean型和日期时间类型
                布尔型会转成整型0和1的形式
                格式为:"YYYY-MM-DD"会使用TEXT存储类
                格式为: UNIX时间戳使用INTEGER存储类
    }

    数据库 {
        创建数据库
            sqlite3 新数据库文件路径

        删除数据库
            rm 数据库文件路径 -f

        附加数据库
            格式：ATTACH DATABASE '要附加数据库的路径' AS 别名;
            如果数据库名称写成':memory:', 代表附加了一个内存数据库,
            内存数据库不会对磁盘操作
            应用场景: 跨库操作
            访问附加数据库的表：附加数据库别名.表名

        分离数据库
            格式：DETACH DATABASE '附加数据库别名'
            对应附加数据库操作，创建多个别名，删除一个别名不会影响其它别名使用
    }

    数据表 {
        创建表
            格式：CREATE TABLE 要创建的表名 (
                    字段名 类型名 约束,
                    ...
                  );

            约束
                PRIMARY KEY         设置主键，主键的数据不能重复, 行的唯一标识

                INTEGER PRIMARY KEY 设置自增主键, 第一条数据必须手动指定
                    当ROW_ID超出最大值，此时会自动搜索没有使用ROW_ID(删除记录时会回收)

                INTEGER PRIMARY KEY AUTOINCREMENT   设置自增字段
                    当此字段累加超过最大值时，会报错

                NOT NULL            字段数据为NULL值, 不能是一个不确定的值
                DEFAULT             字段默认值
                    保存记录创建时间
                        create_time int DEFAULT current_timestamp
                UNIQUE              规定字段数据不能重复
                COLLATE NOCASE      排序忽略大小写
                CHECK               设置值的条件

                外键约束
                    sqlite3默认没有开启外键约束功能，需要手动开启
                        pragma foreign_keys = on;

                    某个字段保存别一个表的主键, 此字段为另一个表的外键
                    cid integer REFERENCES class(id)
                    ON DELETE|UPDATE [CASCADE|SET NULL|SET DEFAULT|RESTRICT|ON ACTION]
                    DEFERRABLE INITIALLY DEFERRED|IMMEDIATE

                    父值删除或更新子值要做什么(规则):
                        set null
                            父值删除，子值全改为null
                        set default
                            父值删除，子值全改为字段默认值
                        cascade
                            更新父值时，更新所有子值
                            删除父值，删除所有子值
                        restrict
                            更新或删除父值，阻止操作
                        no action
                            父值更新或删除不影响子值

                    DEFERRABLE INITIALLY 控制定义约束强制实施还是延迟到事务完成后执行
                        DEFERRED    延迟执行
                        IMMEDIATE   强制执行

            例：创建学生信息表
                CREATE TABLE student (
                    id INTEGER PRIMARY KEY,
                    name TEXT NOT NULL,
                    sex INTEGER DEFAULT 1,
                    age INTEGER CHECK(age >= 18),
                    phone TEXT NOT NULL UNIQUE,
                    cid INTEGER NOT NULL
                );

            根据查询结果创建表/临时表
                格式: CREATE [TEMP] TABLE 新表名[(列名 类型名 约束)] AS SELECT子句;
                创建学生信息的临时表
                    CREATE TEMP TABLE new_student AS SELECT * FROM student;

        修改表
            修改表名称
                ALTER TABLE 表名 RENAME TO 新表名;

            追加字段
                ALTER TABLE 表名 ADD COLUMN 字段名 类型名 约束;

        删除表
            DROP TABLE 表名;

        添加数据
            按指定列名添加对应的值
                INSERT INTO 表名(列名...) VALUES (值...);

            给所有列添加对应的值, 确保值的顺序与列的顺序一致
                INSERT INTO 表名 VALUES(值...);

            将SELECT查询结果添加到表中
                INSERT INTO 表名 [(列名...)] SELECT查询子句;

        更新数据
            更新所有指定字段的数据
                UPDATE 表名 SET 列名=修改值...;

            按WHERE子句更新字段数据
                UPDATE 表名 SET 列名=修改值... WHERE子句;

        删除数据
            删除表里所有数据
                DELETE FROM 表名;

            按WHERE子句删除数据
                DELETE FROM 表名 WHERE子句;

        清空表
            删除重建表
    }

    数据查询 {
        SELECT 是DQL的核心命令, SQL语言中最复杂的命令
        SELECT 提供混合、比较和过滤数据的关系操作
        SELECT通用格式:
            SELECT [DISTINCT] 字段列表      去重和指定要处理的字段名
            FROM 表名                       指定操作的表名
            WHERE 条件                      设置逻辑表达式要过滤行
            GROUP BY 列名                   设置指定分组
            HAVING 条件                     对分组设置逻辑表达过滤
            ORDER BY 列名                   按指定的列排序
            Limit 起始数，限制数量;         限制返回结果的数量

        SELECT从FROM子句开始接受一个或多个输入关系组合单一复合关系传递后续操作链
        除了SELECT，其它子句都是可选

        SELECT常用格式：
            SELECT 字段列表 FROM 表名 WHERE 条件;

        取指定表的所有数据
            SELECT * FROM 表名;

        取指定表的指定字段
            SELECT id,name FROM 表名;

        别名
            SELECT 字段名 AS 别名... FROM 表名 AS 表别名;
            或
            SELECT 字段名 AS 别名... FROM 表名 表别名;

            表的别名在多表查询中非常有用

        过滤(WHERE子句)
            WHERE子句连接逻辑表达式，如果表达式为假，此行数据会被过滤

            SQLite运算符 {
                算术运算符: + - * / %

                比较运算符: = == != <> > < >= <= !< !>
                    = / ==      是否相等
                    != / <>     是否不相等
                    !<          是否不小于
                    !>          是否不大于

                逻辑运算符：
                    AND         逻辑与
                    OR          逻辑或

                    BETWEEN     判断值是否在一个范围内
                        SELECT * FROM student WHERE age BETWEEN 10 AND 30;
                    EXISTS      判断子查询是否有结果
                        SELECT * FROM student s WHERE
                            EXISTS (SELECT id FROM class WHERE id=s.cid)

                    IN          判断某个值是否存在于某个结果中
                        SELECT * FROM student WHERE name IN ('小明', '小东');
                        SELECT * FROM student WHERE class_name
                            IN (SELECT name FROM class);

                    LIKE        判断某个值与使用通配符相似值比较(模糊匹配)
                        _       代表任意一个字符
                        %       代表0个或多个字符

                        SELECT * FROM student WHERE name LIKE '张_%';

                    GLOB        与LIKE功能类似，不同的是通配符不一样和大小敏感
                        ?       代表任意一个字符
                        *       代表0个或多个字符

                        SELECT * FROM student WHERE name GLOB '张?*';

                    IS          相当于=功能

                    IS NOT      相当于!=功能

                    IS NULL     判断某个值是否NULL值

                    ||          连接两个字符串

                    NOT         取非的意思
                        NOT IN          某个值不存在于某个结果中
                        NOT EXISTS      某个子查询没有结果
                        NOT LIKE        某个值与通配符相似值比较，取假
                        NOT BETWEEN     某个值不存某个范围
                        ...

                位运算符: & | ~ >> <<
            }

        限定(LIMIT子句)
            LIMIT子句限定结果集的大小或范围
            从第6开始取10条记录
                SELECT * FROM student LIMIT 10 OFFSET 5;
                或
                SELECT * FROM student LIMIT 5,10;

        排序(ORDER BY子句)
            ORDER BY子句使结果集按一个或多个字段进行排序
                ASC         默认为升序
                DESC        降序

            按姓名升序
                SELECT * FROM student ORDER BY name;

            按姓名降序
                SELECT * FROM student ORDER BY name DESC;

            按多字段降序
                SELECT * FROM student ORDER BY age, name, sex DESC;

        函数和聚合
            SQLite提供内置函数和聚合
            函数名聚合名不区分大小写
            函数可以接受字段作为参数
            函数绝对有返回值，所以可以在任意表达式中使用
            常用函数:
                upper       将字符串转为大写
                lower       将字符串转为小写
                length      求字符串长度
                abs         求绝对值
                random      返回随机数

                日期时间函数
                    date        换算日期 YYYY-MM-DD
                        date('now');    获取当前日期
                        date('now', '+1 day'); 获取当前日期后一天
                    time        换算时间 HH:MM:SS
                    datetime    换算日期时间 YYYY-MM-DD HH:MM:SS:SSS
                    strftime    自定义格式, 包括date time datetime的所有功能
                        strftime("%Y-%m-%d");
                        strftime("%Y-%m-%d", 'now');
                        strftime("%Y-%m-%d", 'now', '1 month');

                    'now'   代表当前日期时间, 可以替换为时间戳

                    修饰符:
                        +/- NNN days        加减天
                        +/- NNN hours       加减小时
                        +/- NNN minutes     加减分钟
                        +/- NNN seconds     加减秒
                        +/- NNN months      加减月
                        +/- NNN years       加减年
                        start of month      从本月开始
                        start of year       从本年开始
                        start of day        从本日开始
                        weekday N           指定星期几的日期
                        unixepoch           指定为UNIX时间戳
                        localtime           本地时区

                    本月第一个星期二的日期
                        date('now', 'start of month', 'weekday 2')

                    当前月的最后一天
                        date('now', 'start of month', '1 month', '-1 day');
                    转化UNIX时间戳为YYYY-MM-DD格式
                        date(1431158143, 'unixepoch', 'localtime');

            聚合是一类特殊的函数，计算一组记录的聚合值
                聚合不仅可以聚合字段，还可以聚合任何表达式，包括函数
                sum         求和
                avg         求平均数
                max         求最大值
                min         求最小值
                count       求结果集行数

        分组(group by)
            group by按指定一个或多个字段进行分组
            分组进行数据处理(统计)
            group by子句必须放在where子句后，在order by子句前

            分组统计第一个班级男女人数
                select sex, count(sex) from student where cid=1 group by sex;
            分组统计所有学校的学生人数
                select sid sum(num) from class group by sid;

            having子句
                having子句对分组结果过滤，用法与where子句类似
                必须group by放后面

                分级统计大于10个人的班级人数
                select cid count(cid) as num from student
                    group by cid having num > 10;

        去重(distinct)
            distinct 过滤结果集中重复的行
            将性别与年龄组合重复的记录合并成一条记录
                select distinct sex,age from student;

        多表连接查询 {
            内连接(join 或 inner join)   两个表都要满足某个条件组合(最为普通)
                查询班级信息
                    select s.name as school, c.name as class,
                           c.num as class_num
                   from class as c, school s
                   where c.sid = s.id;

                   或

                   select s.name as school, c.name as class,
                           c.num as class_num
                   from class as c
                   inner join school s on s.id=c.sid;

                   或

                   select s.name as school, c.name as class,
                           c.num as class_num
                   from class as c
                   join school s on s.id=c.sid;

            交叉连接
                将多个表数据互相组合, 可以通过where子句进行过滤
                select * from class cross join school;

            外连接
                左外连接(left join 或 left outer join)
                    A left join B
                    以A表为基础的查询结果，B表指定数据在A表存在则显示行数据，否则显示null
                    以class表为基础，显示school表与class表id相同的数据
                        select * from class c left join school s on c.id=s.id;

                右外连接(right join 或 right outer join)
                    sqlite不支持
                    A right join B
                    以B表为基础的查询结果，A表指定数据在B表存在则显示行数据，否则显示null

                全外连接(full join 或 full outer join)
                    sqlite不支持
                    A表有，B表没有显示null
                    B表有，A表没有显示null
        }

        复合查询 {
            联合    union
                联合两个查询语句的数据
                    select c.name from class c
                    union all
                    select s.name from school s;

                联合两个查询语句不重复的数据
                    select c.name from class c
                    union
                    select s.name from school s;

            交叉    intersect
                取两个表相同的数据
                    select c.name from class c
                    intersect
                    select s.name from school s;

            差集    except
                取出except关键词前面查询结果在后面查询结果没有的不重复数据
                    select c.name from class c
                    except
                    select s.name from school s;
        }

        条件结果
            根据值不同显示不同的结果
            select name,
                case sex
                    when 1 then '男'
                    when 0 then '女'
                    else null
                end as sex
            from student;

        子查询
            子查询即是select语句嵌套

            输出第一中学的所有学生信息
            select * from student
            where
                cid in (select id from class c
                            join school s on s.id=c.sid
                            and s.name = '第一中学');

        处理NULL
            NULL值是特殊占位符，不是0，不是逻辑真，不是逻辑假，也不是空字符串
            与NULL逻辑:
                TRUE and NULL   结果为NULL
                TRUE or NULL    结果为TRUE
                FALSE and NULL  结果为FALSE
                FALSE or NULL   结果为NULL

            IS NULL 是否为NULL值 / IS NOT NULL 是否不为NULL值

            select * from student where name is null;
    }
}

视图 {
    视图即是虚拟表(派生表)
    与基本表一样，但视图的内容是动态产生(访问视图会更新数据)
    创建视图格式：
        CREATE VIEW 视图名 AS SELECT子句;

    视图作用：
        简化经常使用的查询语句
        将经常运行的查询整理成视图提高查询效率

    创建学生所有信息的视图
        create view v_student as
        select s.name as school, c.name as class, stu.name as name,
                stu.age as age,
                case stu.sex when 1 then '男' else '女' end as sex,
                stu.phone
        from student stu
        join class c on c.id=stu.cid
        join school s on s.id=c.sid;

    使用视图:
        select * from v_student;

    删除视图:
        drop view 视图名称;
}

索引 {
    索引用于某种条件下加速查询的结构

    索引应用场景:
        当查询特定的值，逐行扫描匹配此值, 如果数据量大，此操作又频繁

    索引的问题：
        索引会增加数据库大小, 复制索引列数据进行数据结构
        如果对多列设置索引则使表空间翻倍增加
        当对表进行insert/update/delete操作，也要对索引列进行对应操作
        会降低insert/update/delete的效率

    避免使用索引的情况
        较小的表
        频繁大批量的更新或插入操作的表
        含有大量的NULL值的列
        频繁操作的列

    创建索引格式：
        CREATE INDEX [UNIQUE] 索引名 ON 表名 (要创建索引的列名...);

        对学生表姓名创建索引:
            create index i_stu_name on student (name);

    索引的使用
        当查询中对索引列字段做以下条件判断会自动索引:
            列名 {=|>|>=|<=|<} 值
            或
            列名 IN (列表或子查询)

        当设置多列索引，使用规则必须是从左向右，中间不能间断
        设置三个字段索引， 不能使用第一个索引字段，再使用第三个索引字段
}

事务 {
    事务是对数据库执行工作单元，使用事务可以确保数据的完整性
    事务是将一些SQL组合起来一起执行

    事务具有四个标准属性(ACID)
        原子性 (Atomicity)
            确定工作单元的所有操作都成功完成,
            否则事务会在出现故障时终止，
            之前成功的操作也会回滚到以前状态
        一致性 (Consistency)
            确保数据库在成功提交事务才正确的改变状态
        隔离性 (Isolation)
            使事务操作相互独立和透明
        持久性 (Durability)
            确定已提交事务的结果或效果在系统发生故障的情况下仍然存在

    事务控制命令
        BEGIN/BEGIN TRANSACTION     开始事务处理
        COMMIT/END TRANSACTION      提交事务，保存更改
        ROLLBACK                    回滚所做的更改

    例：
        begin;
        delete * from school;
        rollback;
        select * from school;

        或

        begin;
        delete * from school;
        commit;
        select * from school;
}

触发器 {
    当具体的表发生特定的数据库事件时，触发器执行对应的SQL语句(数据库回调函数)
    创建触发器格式:
        CREATE [TEMP] TRIGGER 触发器名称 [BEFORE|AFTER]
        [INSERT|DELETE|UPDATE|UPDATE OF COLUMNS] ON 表名
        BEGIN
            ACTION
        END;

        BEFORE/AFTER        是在事件发生前还是发生后执行动作
        事件类型： INSERT|DELETE|UPDATE|UPDATE OF COLUMNS
        ACTION      事件产生要执行的操作

    触发器关联表删除时，触发器也会自动删除
    触发器只能针对本库的表

    删除触发器: DORP TRIGGER 触发器名;
}

PHP操作SQLite {
    PHP内置sqlite扩展, 编译默认是开启sqlite3扩展

    ubuntu安装：
        apt install php7.0-sqlite3

    SQLite3类为操作sqlite3的接口
        构造函数    传递操作数据库路径
        exec        执行sql语句(DDL DML)
        query       执行sql查询语句(DQL)
        querySingle 执行sql查询语句返回结果的第一行第一列的数据
        lastInsertRowID     返回最后插入数据的行标识符
        createFunction      创建自定义函数
        createAggregate     创建自定义聚合
        lastErrorCode       返回最后执行SQL语句的错误码
        lastErrorMsg        返回最后执行SQL语句的错误信息
        prepare     对sql语句进行预处理
            返回SQLite3Stmt类的对象
                bindParam       将变量引用绑定到SQL中
                bindValue       将变量的值绑定到SQL中
                execute         将绑定的变量的值替换到SQL中并且执行
                clear           清除绑定情况
                close           关闭预处理
                reset           重新设置预处理
}

Ubuntu卸载安装PHP7:
    /etc/php
    /usr/lib/php
    /usr/share/php

    卸载PHP7主程序与扩展:
        sudo apt remove php7.0* php-common
    卸载PHP7的配置:
        sudo apt purge php7.0* php-common

    卸载php7依赖包
        sudo apt autoremove

    重新安装php7:
        sudo apt install php php-sqlite3

-------------------------------------------------------------------------------
作业：

输出学生信息：
    学校名称 班级名称 学生姓名 学生年龄 学生性别 学生电话
    select s.name as school, c.name as class, stu.name as name,
            stu.age as age,
            case stu.sex when 1 then '男' else '女' end as sex,
            stu.phone
    from student stu
    join class c on c.id=stu.cid
    join school s on s.id=c.sid
    where stu.age > 16
    order by school, class, name, age, sex;

输出成绩信息:
    学校名称 班级名称 学生姓名 课程名称 分数 时间
    select s.name as school, c.name as class, stu.name as student,
            co.name as course, g.score as score,
            datetime(g.time, 'unixepoch', 'localtime')
    from grade as g
    join student as stu on stu.id=g.stu_id
    join course as co on co.id=g.course_id
    join school as s on s.id=co.sid
    join class as c on c.id=stu.cid;

实现数据库版的学生成绩管理系统

实现数据库版的排课查询系统


