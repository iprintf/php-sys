#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    print_r(gd_info());
    $img = imagecreatetruecolor(480, 480);

    $white = imagecolorallocate($img, 255, 255, 255);
    $red = imagecolorallocate($img, 255, 0, 0);
    $yellow = imagecolorallocate($img, 255, 255, 0);
    $black = imagecolorallocate($img, 0, 0, 0);

    imagefill($img, 0, 0, $white);

    // imagesetpixel($img, 100, 100, $red);
    // imageline($img, 100, 100, 250, 100, $red);
    // imageline($img, 100, 100, 100, 250, $red);
    // imageline($img, 100, 100, 200, 200, $red);
    // imagerectangle($img, 100, 100, 200, 200, $red);
    // imagefilledrectangle($img, 100, 100, 200, 200, $red);
    // imagefilledellipse($img, 100, 100, 50, 100, $red);
    /*
     * imagefilledpolygon($img, array(
     *         100, 100,
     *         100, 200,
     *         300, 200
     * ), 3, $red);
     */

    for ($i = 0; $i < 50; ++$i)
    {
        // imagefilledarc($img, 200, 200 + $i, 200, 100, 90, 0, $red, IMG_ARC_PIE);
        // imagefilledarc($img, 200, 200 + $i, 200, 100, 0, 90, $yellow, IMG_ARC_PIE);
    }
    imagefilledarc($img, 200, 200, 200, 100, 90, 0, $red, IMG_ARC_PIE);
    imagefilledarc($img, 200, 200, 200, 100, 0, 90, $yellow, IMG_ARC_PIE);

    imagestring($img, 3, 220, 220, "25", $black);
    imagestringup($img, 3, 180, 200, "75", $black);

    imagettftext($img, 20, 0, 100, 300, $red, "./Ubuntu-C.ttf", "hello world");
    imagettftext($img, 20, 180, 210, 322, $red, "./Ubuntu-C.ttf", "dlrow olleh");

    $bg = imagecreatefrompng("./logo.png");

    // imagecopy($img, $bg, 100, 100, 100, 100, 72, 72);
    // imagecopymerge($img, $bg, 100, 100, 100, 100, 72, 72, 50);
    $ro = imagerotate($bg, 30, 0);
    imagecopyresampled($img, $ro, 100, 100, 0, 0, 272, 272, 1024, 768);

    imagejpeg($img, "test.jpg");

    imagedestroy($img);

    system("eog test.jpg");

    return 0;
}

exit(main($argc, $argv));

