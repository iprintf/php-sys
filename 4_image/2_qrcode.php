#!/usr/bin/php
<?php
require_once "BaconQrCode/autoload_register.php";

use BaconQrCode\Renderer\Color\Rgb;

function main($argc, & $argv)
{
    $renderer = new \BaconQrCode\Renderer\Image\Png();
    $renderer->setHeight(480);
    $renderer->setWidth(480);
    $renderer->setForegroundColor(new Rgb(255, 0, 0));
    $writer = new \BaconQrCode\Writer($renderer);
    $writer->writeFile('http://baidu.com', 'qrcode.png');

    $size = getimagesize("./qrcode.png");
    $code = imagecreatefrompng("./qrcode.png");
    $logo = imagecreatefromjpeg("./logo.jpg");
    $logo_bg = imagecreatetruecolor(48, 48);

    imagecopyresampled($logo_bg, $logo, 0, 0, 0, 0, 48, 48, 650, 477);

    imagecopymerge($code, $logo_bg,
        ($size[0] - 28) / 2, ($size[1] - 28) / 2,
        0, 0, 48, 48, 50);

    imagepng($code, "./qrcode.png");

    imagedestroy($logo_bg);
    imagedestroy($code);
    imagedestroy($logo);

    system("eog qrcode.png");

    return 0;
}

exit(main($argc, $argv));

