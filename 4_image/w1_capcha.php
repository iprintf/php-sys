#!/usr/bin/php
<?php

const W = 200;
const H = 80;

function colors($img, $num = 255)
{
    $color = array();

    for ($i = 0; $i < $num; ++$i) {
        $color[$i] = imagecolorallocate($img, mt_rand(0, 200),
            mt_rand(0, 200), mt_rand(0, 255));
    }
    return $color;
}

function main($argc, & $argv)
{
    $code = array_merge(range(65, 90), range(97, 122));
    $font = "./Ubuntu-C.ttf";

    $img = imagecreatetruecolor(W, H);

    $bg = imagecolorallocate($img, 255, 255, 255);
    $color = colors($img);

    imagefill($img, 0, 0, $bg);

    $line_color = array();

    for ($i = 0; $i < 4; ++$i) {
        $fontsize = mt_rand(24, 48);
        $ch = chr($code[array_rand($code)]);
        printf("ch = %s\n", $ch);
        $ch_color = $color[array_rand($color)];
        $line_color[] = $ch_color;
        printf("color = %d\n", $ch_color);

        imagettftext($img, $fontsize, mt_rand(-50, 50),
                20 + $fontsize * $i, (int)((H - $fontsize) / 2 + $fontsize),
                $ch_color,
                $font,
                $ch
        );
    }

    $line_num = mt_rand(5, 15);
    for ($i = 0; $i < $line_num; ++$i) {
        imageline($img, 0, mt_rand(0, H), W, mt_rand(0, H), $line_color[array_rand($line_color)]);
    }

    $px_num = mt_rand(100, 1000);
    for ($i = 0; $i < $px_num; ++$i) {
        imagesetpixel($img, mt_rand(0, W), mt_rand(0, H), $line_color[array_rand($line_color)]);
    }

    imagepng($img, "./logo.png");

    imagedestroy($img);

    system("eog logo.png");

    return 0;
}

exit(main($argc, $argv));

