#!/usr/bin/php
<?php

namespace kyo\play;

use \kyo\menu\menu;

require_once(__DIR__.'/menu.php');

class mplayer
{
    const PAUSE = "p";
    const SEEK = "seek";
    const OSD_ON = "osd 3";
    const OSD_OFF = "osd 1";
    const MUTE = "mute";
    const TIME_LEN = "get_time_length";
    const TIME_POS = "get_time_pos";
    const FAST_FORWARD = "seek 5";
    const FAST_BACKWARD = "seek -5";
    const VOL_ADD = "volume +10";
    const VOL_SUB = "volume -10";
    const QUIT = "q";

    private $menu = null;
    private $osd = mplayer::OSD_ON;
    private $command = array();
    private $ctl_name = "";
    private $ctl = null;
    private $pid = 0;
    private $mes = null;

    public function __construct()
    {
        $this->menu = new menu(array(
            'show' => array(__NAMESPACE__."\mplayer", "show"),
            'handle' => array(__NAMESPACE__."\mplayer", "handle"),
            'data' => array(& $this),
            'num' => 11
        ));
        $this->command = array(
            mplayer::PAUSE,
            mplayer::FAST_FORWARD,
            mplayer::FAST_BACKWARD,
            mplayer::VOL_ADD,
            mplayer::VOL_SUB,
            & $this->osd,
            mplayer::MUTE
        );

        $this->ctl_name = "/tmp/play_kyo_".mt_rand(0, 99);
        umask(0);
        if (!posix_mkfifo($this->ctl_name, 0660)) {
            fprintf(STDERR, "控制文件创建失败!\n");
            return false;
        }
        $this->ctl = fopen($this->ctl_name, "r+");
        if (!$this->ctl) {
            fprintf(STDERR, "控制文件打开失败!\n");
            return false;
        }

        return true;
    }

    public function quit()
    {
        $this->send(mplayer::QUIT);
        if ($this->mes !== null)
            pclose($this->mes);
        if ($this->ctl !== null)
            fclose($this->ctl);
        unlink($this->ctl_name);
        return TRUE;
    }

    public function send($command)
    {
        if ($this->ctl == null)
            return;

        if (strpos($command, "osd ") !== FALSE)
        {
            if ($this->osd == mplayer::OSD_OFF)
                $this->osd = mplayer::OSD_ON;
            else
                $this->osd = mplayer::OSD_OFF;
        }
        fwrite($this->ctl, $command.PHP_EOL);
    }


    public function open()
    {
        printf("\t视频路径(fa.wmv): ");
        $path = trim(fgets(STDIN));
        if (strlen($path) == 0)
            $path = "/kyo/fa.wmv";
        if (!file_exists($path)) {
            fprintf(STDERR, "\t输入的视频路径有误!\n");
            return;
        }
        if ($this->mes !== null) {
            $this->send(mplayer::QUIT);
            pclose($this->mes);
        }
        $this->mes = popen("mplayer $path -slave -quiet -input file=".$this->ctl_name." 2> /dev/null", "r");
        if (!$this->mes) {
            fprintf(STDERR, "\t播放失败!\n");
            return;
        }
        sleep(1);
        fread($this->mes, 8192);
        $this->send($this->osd);
    }

    public function get($command)
    {
        $this->send($command);
        echo fread($this->mes, 8192);
    }


    public function handle($n, $obj)
    {
        switch ($n) {
            case 1:
                $obj->open();
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                $obj->send($obj->command[$n - 2]);
                break;
            case 9:
                echo $obj->get(mplayer::TIME_LEN), PHP_EOL;
                break;
            case 10:
                echo $obj->get(mplayer::TIME_POS), PHP_EOL;
                break;
            case 11:
            default:
                return $obj->quit();
        }
        return FALSE;
    }

    public function show($obj)
    {
        echo "==== 播放器 ====", PHP_EOL;
        echo "1. 打开", PHP_EOL;
        echo "2. 播放/暂停", PHP_EOL;
        echo "3. 快进", PHP_EOL;
        echo "4. 快退", PHP_EOL;
        echo "5. 音量加", PHP_EOL;
        echo "6. 音量减", PHP_EOL;
        if ($obj->osd == mplayer::OSD_ON)
            echo "7. 显示时间进度", PHP_EOL;
        else
            echo "7. 关闭时间进度", PHP_EOL;
        echo "8. 开启/关闭静音", PHP_EOL;
        echo "9. 获取总长度", PHP_EOL;
        echo "10. 获取当前播放时间", PHP_EOL;
        echo "11. 退出", PHP_EOL;
        echo "请选择[1 - 11]: ";
    }

    public function run()
    {
        $this->menu->show();
    }

}


function main($argc, & $argv)
{
    $play = new mplayer();
    if ($play)
        $play->run();

    return 0;
}

exit(main($argc, $argv));

