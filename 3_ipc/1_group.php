#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    printf("pid = %d, ppid = %d, pgrp = %d, sid = %d\n",
            posix_getpid(), posix_getppid(),
            posix_getpgrp(), posix_getsid(0));

    if (pcntl_fork() == 0) {
        printf("child pid = %d, ppid = %d, pgrp = %d, sid = %d\n",
                posix_getpid(), posix_getppid(),
                posix_getpgrp(), posix_getsid(0));
        printf("create sid = %d\n", posix_setsid());

        sleep(1);

        printf("child pid = %d, ppid = %d, pgrp = %d, sid = %d\n",
                posix_getpid(), posix_getppid(),
                posix_getpgrp(), posix_getsid(0));

        // fclose(STDIN);
        // fclose(STDOUT);
        // fclose(STDERR);
        // chdir("/");

        while (true) {
            if (!strncmp(file_get_contents("/tmp/kyo"), "exit", 4))
                break;
            sleep(1);
        }
        exit(0);
    }
    // pcntl_wait($status);

    // fgets(STDIN);

    return 0;
}

exit(main($argc, $argv));

