#!/usr/bin/php5
<?php

function test($str, $obj)
{
    $count = 0;
    while ($count < 10) {
        if ($count == 3 && $obj != null) {
            $obj->kill();
        }

        printf("%s count = %d\n", $str, $count++);
        sleep(1);
    }
}

class kyo extends Thread
{
    private $name = null;
    private $obj = null;

    public function __construct($name = "thread", $obj = null)
    {
        $this->name = $name;
        $this->obj = $obj;
    }

    public function run()
    {
        test($this->name, $this->obj);
    }
}

function main($argc, & $argv)
{
    $v = new kyo();
    $v1 = new kyo("thread2", $v);
    $v2 = new kyo("thread3");
    $v->start();
    $v1->start();
    $v2->start();

    test("main", null);

    $v->join();
    $v1->join();
    $v2->join();

    return 0;
}

exit(main($argc, $argv));

