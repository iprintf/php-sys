#!/usr/bin/php
<?php

declare(ticks=1);

function sig_handle($sig)
{
    echo "signal = ", $sig, PHP_EOL;
}
function main($argc, & $argv)
{
    printf("pid = %d\n", posix_getpid());

    for ($i = 1; $i < 32; ++$i) {
        if (pcntl_signal($i, "sig_handle"))
            continue;
        printf("failed signal = %d\n", $i);
    }

    while (1) {
        usleep(10000);
    }

    return 0;
}

exit(main($argc, $argv));

