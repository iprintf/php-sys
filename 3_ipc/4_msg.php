#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $msg = msg_get_queue(ftok("/kyo", "A"));
    if (!$msg) {
        fprintf(STDERR, "create msg failed!\n");
        return 0;
    }

    if ($argc < 1)
        return 0;

    if ($argv[1] === "d") {
        return msg_remove_queue($msg);
    }
    if ($argc > 2) {
        printf("msg_send = %d\n",  msg_send($msg, $argv[1], $argv[2]));
    } else {
        msg_receive($msg, $argv[1], $type, 1024, $message);
        printf("type = %d, recv: %s\n", $type, $message);
    }

    return 0;
}

exit(main($argc, $argv));

