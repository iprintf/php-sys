#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $shm = shm_attach(ftok("/kyo", "B"));
    if (!$shm) {
        fprintf(STDERR, "create shm failed!\n");
        return 0;
    }

    if ($argc > 1 && $argv[1] === "d") {
        return shm_remove($shm);
    }

    if (pcntl_fork() == 0) {
        shm_put_var($shm, 1, "hello world");
        shm_detach($shm);
        exit(0);
    }
    pcntl_wait($status);

    if (shm_has_var($shm, 1))
        printf("parent: %s\n", shm_get_var($shm, 1));

    shm_detach($shm);

    return 0;
}

exit(main($argc, $argv));

