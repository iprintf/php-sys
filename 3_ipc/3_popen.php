#!/usr/bin/php
<?php

function main($argc, & $argv)
{
    $fp = fopen('./fifo', "r+");
    printf("fwrite = %d\n", fwrite($fp, "p\n"));
    fclose($fp);

    $fr = popen("cat /etc/passwd", "r");
    $fw = popen("grep root --color", "w");

    while (($s = fread($fr, 1024)) != FALSE) {
        fwrite($fw, $s);
    }

    pclose($fr);
    pclose($fw);

    return 0;
}

exit(main($argc, $argv));

